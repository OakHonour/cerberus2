#ifndef CCPP_H_FBDEE196031BD3855CB633E69DD06CFA_Num__H
#define CCPP_H_FBDEE196031BD3855CB633E69DD06CFA_Num__H

#include "Num_.h"
#undef OS_API
#include "Num_Dcps.h"
#undef OS_API

#include <orb_abstraction.h>
#include "Num_Dcps_impl.h"

#endif /* CCPP_H_FBDEE196031BD3855CB633E69DD06CFA_Num__H */
