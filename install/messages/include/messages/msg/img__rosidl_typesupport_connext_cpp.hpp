// generated from rosidl_typesupport_connext_cpp/resource/idl__rosidl_typesupport_connext_cpp.hpp.em
// with input from messages:msg/Img.idl
// generated code does not contain a copyright notice


#ifndef MESSAGES__MSG__IMG__ROSIDL_TYPESUPPORT_CONNEXT_CPP_HPP_
#define MESSAGES__MSG__IMG__ROSIDL_TYPESUPPORT_CONNEXT_CPP_HPP_

#include "rosidl_generator_c/message_type_support_struct.h"
#include "rosidl_typesupport_interface/macros.h"
#include "messages/msg/rosidl_typesupport_connext_cpp__visibility_control.h"
#include "messages/msg/img__struct.hpp"

#ifndef _WIN32
# pragma GCC diagnostic push
# pragma GCC diagnostic ignored "-Wunused-parameter"
# ifdef __clang__
#  pragma clang diagnostic ignored "-Wdeprecated-register"
#  pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
# endif
#endif

#include "messages/msg/dds_connext/Img_Support.h"
#include "messages/msg/dds_connext/Img_Plugin.h"
#include "ndds/ndds_cpp.h"

#ifndef _WIN32
# pragma GCC diagnostic pop
#endif

// forward declaration of internal CDR Stream
struct ConnextStaticCDRStream;

// forward declaration of DDS types
class DDSDomainParticipant;
class DDSDataWriter;
class DDSDataReader;


namespace messages
{

namespace msg
{
namespace typesupport_connext_cpp
{

DDS_TypeCode *
get_type_code__Img();

bool
ROSIDL_TYPESUPPORT_CONNEXT_CPP_PUBLIC_messages
convert_ros_message_to_dds(
  const messages::msg::Img & ros_message,
  messages::msg::dds_::Img_ & dds_message);

bool
ROSIDL_TYPESUPPORT_CONNEXT_CPP_PUBLIC_messages
convert_dds_message_to_ros(
  const messages::msg::dds_::Img_ & dds_message,
  messages::msg::Img & ros_message);

bool
to_cdr_stream__Img(
  const void * untyped_ros_message,
  ConnextStaticCDRStream * cdr_stream);

bool
to_message__Img(
  const ConnextStaticCDRStream * cdr_stream,
  void * untyped_ros_message);

}  // namespace typesupport_connext_cpp

}  // namespace msg

}  // namespace messages


#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_CONNEXT_CPP_PUBLIC_messages
const rosidl_message_type_support_t *
  ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(
  rosidl_typesupport_connext_cpp,
  messages, msg,
  Img)();

#ifdef __cplusplus
}
#endif


#endif  // MESSAGES__MSG__IMG__ROSIDL_TYPESUPPORT_CONNEXT_CPP_HPP_
