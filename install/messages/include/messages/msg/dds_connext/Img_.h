

/*
WARNING: THIS FILE IS AUTO-GENERATED. DO NOT MODIFY.

This file was generated from Img_.idl using "rtiddsgen".
The rtiddsgen tool is part of the RTI Connext distribution.
For more information, type 'rtiddsgen -help' at a command shell
or consult the RTI Connext manual.
*/

#ifndef Img__632199926_h
#define Img__632199926_h

#ifndef NDDS_STANDALONE_TYPE
#ifndef ndds_cpp_h
#include "ndds/ndds_cpp.h"
#endif
#else
#include "ndds_standalone_type.h"
#endif

namespace messages {
    namespace msg {
        namespace dds_ {

            extern const char *Img_TYPENAME;

            struct Img_Seq;
            #ifndef NDDS_STANDALONE_TYPE
            class Img_TypeSupport;
            class Img_DataWriter;
            class Img_DataReader;
            #endif

            class Img_ 
            {
              public:
                typedef struct Img_Seq Seq;
                #ifndef NDDS_STANDALONE_TYPE
                typedef Img_TypeSupport TypeSupport;
                typedef Img_DataWriter DataWriter;
                typedef Img_DataReader DataReader;
                #endif

                DDS_Float   xtop_ ;
                DDS_Float   xbot_ ;
                DDS_Float   ytop_ ;
                DDS_Float   ybot_ ;
                DDS_Octet   name_ ;

            };
            #if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
            /* If the code is building on Windows, start exporting symbols.
            */
            #undef NDDSUSERDllExport
            #define NDDSUSERDllExport __declspec(dllexport)
            #endif

            NDDSUSERDllExport DDS_TypeCode* Img__get_typecode(void); /* Type code */

            DDS_SEQUENCE(Img_Seq, Img_);

            NDDSUSERDllExport
            RTIBool Img__initialize(
                Img_* self);

            NDDSUSERDllExport
            RTIBool Img__initialize_ex(
                Img_* self,RTIBool allocatePointers,RTIBool allocateMemory);

            NDDSUSERDllExport
            RTIBool Img__initialize_w_params(
                Img_* self,
                const struct DDS_TypeAllocationParams_t * allocParams);  

            NDDSUSERDllExport
            void Img__finalize(
                Img_* self);

            NDDSUSERDllExport
            void Img__finalize_ex(
                Img_* self,RTIBool deletePointers);

            NDDSUSERDllExport
            void Img__finalize_w_params(
                Img_* self,
                const struct DDS_TypeDeallocationParams_t * deallocParams);

            NDDSUSERDllExport
            void Img__finalize_optional_members(
                Img_* self, RTIBool deletePointers);  

            NDDSUSERDllExport
            RTIBool Img__copy(
                Img_* dst,
                const Img_* src);

            #if (defined(RTI_WIN32) || defined (RTI_WINCE)) && defined(NDDS_USER_DLL_EXPORT)
            /* If the code is building on Windows, stop exporting symbols.
            */
            #undef NDDSUSERDllExport
            #define NDDSUSERDllExport
            #endif
        } /* namespace dds_  */
    } /* namespace msg  */
} /* namespace messages  */

#endif /* Img_ */

