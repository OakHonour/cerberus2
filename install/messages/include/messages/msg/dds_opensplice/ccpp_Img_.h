#ifndef CCPP_H_36B68CA0F2C9331561F0FF773C898AF3_Img__H
#define CCPP_H_36B68CA0F2C9331561F0FF773C898AF3_Img__H

#include "Img_.h"
#undef OS_API
#include "Img_Dcps.h"
#undef OS_API

#include <orb_abstraction.h>
#include "Img_Dcps_impl.h"

#endif /* CCPP_H_36B68CA0F2C9331561F0FF773C898AF3_Img__H */
