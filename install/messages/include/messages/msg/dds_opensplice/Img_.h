#ifndef _H_36B68CA0F2C9331561F0FF773C898AF3_Img__H_
#define _H_36B68CA0F2C9331561F0FF773C898AF3_Img__H_

#include "sacpp_mapping.h"
#include "cpp_dcps_if.h"


namespace messages
{
    namespace msg
    {
        namespace dds_
        {
            struct  Img_
            {
                ::DDS::Float xtop_;
                ::DDS::Float xbot_;
                ::DDS::Float ytop_;
                ::DDS::Float ybot_;
                ::DDS::Octet name_;
            };

            typedef DDS_DCPSStruct_var<Img_> Img__var;
            typedef Img_& Img__out;

        }

    }

}

#endif /* _H_36B68CA0F2C9331561F0FF773C898AF3_Img__H_ */
