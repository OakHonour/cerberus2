#ifndef H_36B68CA0F2C9331561F0FF773C898AF3_Img_SPLTYPES_H
#define H_36B68CA0F2C9331561F0FF773C898AF3_Img_SPLTYPES_H

#include <c_base.h>
#include <c_misc.h>
#include <c_sync.h>
#include <c_collection.h>
#include <c_field.h>
#include <v_copyIn.h>

#include "ccpp_Img_.h"


extern c_metaObject __Img__messages__load (c_base base);

extern c_metaObject __Img__messages_msg__load (c_base base);

extern c_metaObject __Img__messages_msg_dds___load (c_base base);

extern const char *messages_msg_dds__Img__metaDescriptor[];
extern const int messages_msg_dds__Img__metaDescriptorArrLength;
extern const int messages_msg_dds__Img__metaDescriptorLength;
extern c_metaObject __messages_msg_dds__Img___load (c_base base);
struct _messages_msg_dds__Img_ ;
extern  v_copyin_result __messages_msg_dds__Img___copyIn(c_base base, const struct messages::msg::dds_::Img_ *from, struct _messages_msg_dds__Img_ *to);
extern  void __messages_msg_dds__Img___copyOut(const void *_from, void *_to);
struct _messages_msg_dds__Img_ {
    c_float xtop_;
    c_float xbot_;
    c_float ytop_;
    c_float ybot_;
    c_octet name_;
};

#undef OS_API
#endif
