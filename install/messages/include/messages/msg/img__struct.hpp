// generated from rosidl_generator_cpp/resource/idl__struct.hpp.em
// with input from messages:msg/Img.idl
// generated code does not contain a copyright notice

#ifndef MESSAGES__MSG__IMG__STRUCT_HPP_
#define MESSAGES__MSG__IMG__STRUCT_HPP_

#include <rosidl_generator_cpp/bounded_vector.hpp>
#include <rosidl_generator_cpp/message_initialization.hpp>
#include <algorithm>
#include <array>
#include <memory>
#include <string>
#include <vector>

// Protect against ERROR being predefined on Windows, in case somebody defines a
// constant by that name.
#if defined(_WIN32)
  #if defined(ERROR)
    #undef ERROR
  #endif
  #if defined(NO_ERROR)
    #undef NO_ERROR
  #endif
#endif

#ifndef _WIN32
# define DEPRECATED__messages__msg__Img __attribute__((deprecated))
#else
# define DEPRECATED__messages__msg__Img __declspec(deprecated)
#endif

namespace messages
{

namespace msg
{

// message struct
template<class ContainerAllocator>
struct Img_
{
  using Type = Img_<ContainerAllocator>;

  explicit Img_(rosidl_generator_cpp::MessageInitialization _init = rosidl_generator_cpp::MessageInitialization::ALL)
  {
    if (rosidl_generator_cpp::MessageInitialization::ALL == _init ||
      rosidl_generator_cpp::MessageInitialization::ZERO == _init)
    {
      this->xtop = 0.0f;
      this->xbot = 0.0f;
      this->ytop = 0.0f;
      this->ybot = 0.0f;
      this->name = 0;
    }
  }

  explicit Img_(const ContainerAllocator & _alloc, rosidl_generator_cpp::MessageInitialization _init = rosidl_generator_cpp::MessageInitialization::ALL)
  {
    (void)_alloc;
    if (rosidl_generator_cpp::MessageInitialization::ALL == _init ||
      rosidl_generator_cpp::MessageInitialization::ZERO == _init)
    {
      this->xtop = 0.0f;
      this->xbot = 0.0f;
      this->ytop = 0.0f;
      this->ybot = 0.0f;
      this->name = 0;
    }
  }

  // field types and members
  using _xtop_type =
    float;
  _xtop_type xtop;
  using _xbot_type =
    float;
  _xbot_type xbot;
  using _ytop_type =
    float;
  _ytop_type ytop;
  using _ybot_type =
    float;
  _ybot_type ybot;
  using _name_type =
    int8_t;
  _name_type name;

  // setters for named parameter idiom
  Type & set__xtop(
    const float & _arg)
  {
    this->xtop = _arg;
    return *this;
  }
  Type & set__xbot(
    const float & _arg)
  {
    this->xbot = _arg;
    return *this;
  }
  Type & set__ytop(
    const float & _arg)
  {
    this->ytop = _arg;
    return *this;
  }
  Type & set__ybot(
    const float & _arg)
  {
    this->ybot = _arg;
    return *this;
  }
  Type & set__name(
    const int8_t & _arg)
  {
    this->name = _arg;
    return *this;
  }

  // constant declarations

  // pointer types
  using RawPtr =
    messages::msg::Img_<ContainerAllocator> *;
  using ConstRawPtr =
    const messages::msg::Img_<ContainerAllocator> *;
  using SharedPtr =
    std::shared_ptr<messages::msg::Img_<ContainerAllocator>>;
  using ConstSharedPtr =
    std::shared_ptr<messages::msg::Img_<ContainerAllocator> const>;

  template<typename Deleter = std::default_delete<
      messages::msg::Img_<ContainerAllocator>>>
  using UniquePtrWithDeleter =
    std::unique_ptr<messages::msg::Img_<ContainerAllocator>, Deleter>;

  using UniquePtr = UniquePtrWithDeleter<>;

  template<typename Deleter = std::default_delete<
      messages::msg::Img_<ContainerAllocator>>>
  using ConstUniquePtrWithDeleter =
    std::unique_ptr<messages::msg::Img_<ContainerAllocator> const, Deleter>;
  using ConstUniquePtr = ConstUniquePtrWithDeleter<>;

  using WeakPtr =
    std::weak_ptr<messages::msg::Img_<ContainerAllocator>>;
  using ConstWeakPtr =
    std::weak_ptr<messages::msg::Img_<ContainerAllocator> const>;

  // pointer types similar to ROS 1, use SharedPtr / ConstSharedPtr instead
  // NOTE: Can't use 'using' here because GNU C++ can't parse attributes properly
  typedef DEPRECATED__messages__msg__Img
    std::shared_ptr<messages::msg::Img_<ContainerAllocator>>
    Ptr;
  typedef DEPRECATED__messages__msg__Img
    std::shared_ptr<messages::msg::Img_<ContainerAllocator> const>
    ConstPtr;

  // comparison operators
  bool operator==(const Img_ & other) const
  {
    if (this->xtop != other.xtop) {
      return false;
    }
    if (this->xbot != other.xbot) {
      return false;
    }
    if (this->ytop != other.ytop) {
      return false;
    }
    if (this->ybot != other.ybot) {
      return false;
    }
    if (this->name != other.name) {
      return false;
    }
    return true;
  }
  bool operator!=(const Img_ & other) const
  {
    return !this->operator==(other);
  }
};  // struct Img_

// alias to use template instance with default allocator
using Img =
  messages::msg::Img_<std::allocator<void>>;

// constant definitions

}  // namespace msg

}  // namespace messages

#endif  // MESSAGES__MSG__IMG__STRUCT_HPP_
