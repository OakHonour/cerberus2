

#ifndef Transmitter
#define Transmitter


class Transmit{

    int serial_port;

    public:
        Transmit();// Constructor
        void send(char cmd);// write command char to arduino serial port
        void stop();// close serial port
};

#endif