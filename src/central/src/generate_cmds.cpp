#include "generate_cmds.h"

Generate::Generate(){

}

char Generate::FindCentre(float topx, float botx){

    char cmd = '1'; //1 - still //2 - rotate clockwise//3 - rotate anti-clockwise
    int boxCentre = botx - ((botx - topx) / 2);
    
    int centre = 300;
    int tolerance = 30;

    //int actualCentre;

    if(boxCentre < centre - tolerance){
        cmd = '2';
    }
    else if(boxCentre > centre + tolerance){
         cmd = '3';
    }
    else{
         cmd = '1';
    }

    return cmd;
}
