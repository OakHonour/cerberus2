
#include <string.h>
#include <iostream>
#include <fstream>
#include <stdio.h> 

#include <fcntl.h> // contains file controls
#include <errno.h> // int erros and std erros funcs
#include <termios.h> // Unix API for terminal I/O
#include <unistd.h> // read, write and close

#include "central_transmit.h"


    Transmit::Transmit(){

        serial_port = open("/dev/ttyACM0", O_RDWR); // open port file as read/write
        if(serial_port < 0){ // 
            printf("Error %i from open %s\n", errno, strerror(errno));
        }

        struct termios tty;
        memset(&tty, 0, sizeof tty);

        if(tcgetattr(serial_port, &tty) != 0){
            printf("Error %i from tcgeattr: %s\n", errno, strerror(errno));
        }

        cfsetispeed(&tty, B9600); // Set baud rate input and output
        cfsetospeed(&tty, B9600);

        if (tcsetattr(serial_port, TCSANOW, &tty) != 0) {
            printf("Error %i from tcsetattr: %s\n", errno, strerror(errno));
        }

     }

     void Transmit::send(char cmd){
         const void* cvp_cmd = &cmd;
        write(serial_port, cvp_cmd, 1);
     }

     void Transmit::stop(){
         close(serial_port);
     }