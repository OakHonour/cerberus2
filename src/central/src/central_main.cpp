#include <memory>

#include "rclcpp/rclcpp.hpp"
#include "std_msgs/msg/string.hpp"
#include "std_msgs/msg/float32.hpp"
#include "messages/msg/img.hpp"


#include <string.h>
#include <iostream>
#include <fstream>
#include <stdio.h>

#include "central_transmit.cpp"
#include "generate_cmds.cpp"

using std::placeholders::_1;
using std::fstream;

Transmit trans;
Generate generator;


class ImageSubscriber : public rclcpp::Node
{
   
  public:
    ImageSubscriber()
    : Node("image_subscriber")
    {
      subscription_ = this->create_subscription<messages::msg::Img>("image_data", 10, bind(&ImageSubscriber::topic_callback, this, _1));
    }

  private:

    void topic_callback(const messages::msg::Img::SharedPtr msg) const
    {

      char cmd = generator.FindCentre(msg->xtop, msg->xbot);
      
      trans.send(cmd);

      RCLCPP_INFO(this->get_logger(), "Command: '%c'", cmd);

      /*
      RCLCPP_INFO(this->get_logger(), "name: '%lf'", msg->name);
      RCLCPP_INFO(this->get_logger(), "xtop: '%lf'", msg->xtop);
      RCLCPP_INFO(this->get_logger(), "ytop: '%lf'", msg->ytop);
      RCLCPP_INFO(this->get_logger(), "xbot: '%lf'", msg->xbot);
      RCLCPP_INFO(this->get_logger(), "ybot: '%lf'", msg->ybot);
      */
    }
    rclcpp::Subscription<messages::msg::Img>::SharedPtr subscription_;
};

int main(int argc, char * argv[])
{

  rclcpp::init(argc, argv);
  rclcpp::spin(std::make_shared<ImageSubscriber>());
  rclcpp::shutdown();
  trans.stop();
  return 0;
}