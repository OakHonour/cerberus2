#ifndef Gen
#define Gen

class Generate{

    public:
        Generate(); // Generator Constructor
        char FindCentre(float topx, float botx);//Works out bounding box centre point and returns appropriate command

};

#endif