#!/usr/bin/env python

from __future__ import division
import time
import torch 
import torch.nn as nn
from torch.autograd import Variable
import numpy as np
import cv2 
from camera.util import *
import argparse
import os 
import os.path as osp
from camera.darknet import Darknet
import pickle as pkl
import pandas as pd
import random

import rclpy
from rclpy.node import Node

from messages.msg import Img


class ImagePublisher(Node):

    def __init__(self):
        super().__init__('image_publisher')
        self.publisher_ = self.create_publisher(Img, 'image_data', 10)
        timer_period = 0.5  # seconds
        #self.timer = self.create_timer(timer_period, self.timer_callback)
        self.i = 0

    
    def timer_callback(self):
        msg = Img()
        msg.data = 1.2
        self.publisher_.publish(msg)
        self.get_logger().info('Publishing: "%s"' % msg.data)
        self.i += 1

def write(x, results):
    c1 = tuple(x[1:3].int())
    c2 = tuple(x[3:5].int())
    img = results
    cls = int(x[-1])
    color = random.choice(colors)
    label = "{0}".format(classes[cls])
    cv2.rectangle(img, c1, c2,color, 1)
    t_size = cv2.getTextSize(label, cv2.FONT_HERSHEY_PLAIN, 1 , 1)[0]
    c2 = c1[0] + t_size[0] + 3, c1[1] + t_size[1] + 4
    cv2.rectangle(img, c1, c2,color, -1)
    cv2.putText(img, label, (c1[0], c1[1] + t_size[1] + 4), cv2.FONT_HERSHEY_PLAIN, 1, [225,255,255], 1);
    return img

batch_size = 1
confidence = 0.5
nms_thesh = 0.1
start = 0
CUDA = torch.cuda.is_available()

num_classes = 80
classes = load_classes("src/camera/camera/data/coco.names")

model = Darknet("src/camera/camera/cfg/yolov3.cfg")
model.load_weights("src/camera/camera/yolov3.weights")
model.net_info["height"] = 416
inp_dim = int(model.net_info["height"])
assert inp_dim % 32 == 0 
assert inp_dim > 32

#If there's a GPU availible, put the model on GPU
if CUDA:
    model.cuda()


#Set the model in evaluation mode
model.eval()

cap = cv2.VideoCapture(0)

assert cap.isOpened(), 'Cannot capture source'

frames = 0

start = time.time()


# Set up ROS
rclpy.init(args=None)
image_publisher = ImagePublisher()

while cap.isOpened():
    ret, frame = cap.read()
    
    if ret:   
        img = prep_image(frame, inp_dim)
#        cv2.imshow("a", frame)
        im_dim = frame.shape[1], frame.shape[0]
        im_dim = torch.FloatTensor(im_dim).repeat(1,2)   
                     
        if CUDA:
            im_dim = im_dim.cuda()
            img = img.cuda()
        
        with torch.no_grad():
            output = model(Variable(img, volatile = True), CUDA)
        output = write_results(output, confidence, num_classes, nms_conf = nms_thesh)


        if type(output) == int:
            frames += 1
            print("FPS of the video is {:5.4f}".format( frames / (time.time() - start)))
            cv2.imshow("frame", frame)
            key = cv2.waitKey(1)
            if key & 0xFF == ord('q'):
                break
            continue
        
        
        

        im_dim = im_dim.repeat(output.size(0), 1)
        scaling_factor = torch.min(416/im_dim,1)[0].view(-1,1)
        
        output[:,[1,3]] -= (inp_dim - scaling_factor*im_dim[:,0].view(-1,1))/2
        output[:,[2,4]] -= (inp_dim - scaling_factor*im_dim[:,1].view(-1,1))/2
        
        output[:,1:5] /= scaling_factor
        # x topleft bottomright
        #print('{}, {}'.format(output[0][1], output[0][3]))
        # y topleft bottomright
        #print('{}, {}'.format(output[0][2], output[0][4]))
        # Class - 0 is person
        #print(output[0][7].item())

        #msg = Image()
        #msg.xtop = output[0][1]
        #msg.xbot = output[0][3]

        #msg.ytop = output[0][2]
        #msg.ybot = output[0][4]

        #msg.class_ = int(output[0][7].item())
        msg = Img()
        msg.xtop = float(output[0][1])
        msg.xbot = float(output[0][3])

        msg.ytop = float(output[0][2])
        msg.ybot = float(output[0][4])
        
        

        
        image_publisher.publisher_.publish(msg)
        image_publisher.i += 1
        #rclpy.spin(minimal_publisher)
    
        
        for i in range(output.shape[0]):
            output[i, [1,3]] = torch.clamp(output[i, [1,3]], 0.0, im_dim[i,0])
            output[i, [2,4]] = torch.clamp(output[i, [2,4]], 0.0, im_dim[i,1])
    
        
        

        classes = load_classes('src/camera/camera/data/coco.names')
        colors = pkl.load(open("src/camera/camera/pallete", "rb"))

        list(map(lambda x: write(x, frame), output))
        
        cv2.imshow("frame", frame)
        key = cv2.waitKey(1)
        if key & 0xFF == ord('q'):
            break
        frames += 1

        #print(time.time() - start)
        #print("FPS of the video is {:5.2f}".format( frames / (time.time() - start)))
    else:
        break   
