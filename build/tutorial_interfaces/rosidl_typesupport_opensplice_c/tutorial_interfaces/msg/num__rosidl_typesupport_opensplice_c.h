// generated from rosidl_typesupport_opensplice_c/resource/idl__rosidl_typesupport_c.h.em
// generated code does not contain a copyright notice
#ifndef TUTORIAL_INTERFACES__MSG__NUM__ROSIDL_TYPESUPPORT_OPENSPLICE_C_H_
#define TUTORIAL_INTERFACES__MSG__NUM__ROSIDL_TYPESUPPORT_OPENSPLICE_C_H_
// generated from
// rosidl_typesupport_opensplice_c/resource/msg__rosidl_typesupport_opensplice_c.h.em
// generated code does not contain a copyright notice

#include "rosidl_generator_c/message_type_support_struct.h"
#include "rosidl_typesupport_interface/macros.h"
#include "tutorial_interfaces/msg/rosidl_typesupport_opensplice_c__visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_OPENSPLICE_C_PUBLIC_tutorial_interfaces
const rosidl_message_type_support_t *
  ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(
  rosidl_typesupport_opensplice_c,
  tutorial_interfaces, msg,
  Num)();

#ifdef __cplusplus
}
#endif
#endif  // TUTORIAL_INTERFACES__MSG__NUM__ROSIDL_TYPESUPPORT_OPENSPLICE_C_H_
