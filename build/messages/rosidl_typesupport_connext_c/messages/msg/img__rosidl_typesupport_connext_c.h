// generated from rosidl_typesupport_connext_c/resource/idl__rosidl_typesupport_connext_c.h.em
// with input from messages:msg/Img.idl
// generated code does not contain a copyright notice


#ifndef MESSAGES__MSG__IMG__ROSIDL_TYPESUPPORT_CONNEXT_C_H_
#define MESSAGES__MSG__IMG__ROSIDL_TYPESUPPORT_CONNEXT_C_H_

#include "rosidl_generator_c/message_type_support_struct.h"
#include "rosidl_typesupport_interface/macros.h"
#include "messages/msg/rosidl_typesupport_connext_c__visibility_control.h"

#ifdef __cplusplus
extern "C"
{
#endif

ROSIDL_TYPESUPPORT_CONNEXT_C_PUBLIC_messages
const rosidl_message_type_support_t *
  ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(
  rosidl_typesupport_connext_c,
  messages, msg,
  Img)();

#ifdef __cplusplus
}
#endif

#endif  // MESSAGES__MSG__IMG__ROSIDL_TYPESUPPORT_CONNEXT_C_H_
