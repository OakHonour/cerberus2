// generated from rosidl_typesupport_fastrtps_c/resource/idl__type_support_c.cpp.em
// with input from messages:msg/Img.idl
// generated code does not contain a copyright notice
#include "messages/msg/img__rosidl_typesupport_fastrtps_c.h"


#include <cassert>
#include <limits>
#include <string>
#include "rosidl_typesupport_fastrtps_c/identifier.h"
#include "rosidl_typesupport_fastrtps_c/wstring_conversion.hpp"
#include "rosidl_typesupport_fastrtps_cpp/message_type_support.h"
#include "messages/msg/rosidl_typesupport_fastrtps_c__visibility_control.h"
#include "messages/msg/img__struct.h"
#include "messages/msg/img__functions.h"
#include "fastcdr/Cdr.h"

#ifndef _WIN32
# pragma GCC diagnostic push
# pragma GCC diagnostic ignored "-Wunused-parameter"
# ifdef __clang__
#  pragma clang diagnostic ignored "-Wdeprecated-register"
#  pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
# endif
#endif
#ifndef _WIN32
# pragma GCC diagnostic pop
#endif

// includes and forward declarations of message dependencies and their conversion functions

#if defined(__cplusplus)
extern "C"
{
#endif


// forward declare type support functions


using _Img__ros_msg_type = messages__msg__Img;

static bool _Img__cdr_serialize(
  const void * untyped_ros_message,
  eprosima::fastcdr::Cdr & cdr)
{
  if (!untyped_ros_message) {
    fprintf(stderr, "ros message handle is null\n");
    return false;
  }
  const _Img__ros_msg_type * ros_message = static_cast<const _Img__ros_msg_type *>(untyped_ros_message);
  // Field name: xtop
  {
    cdr << ros_message->xtop;
  }

  // Field name: xbot
  {
    cdr << ros_message->xbot;
  }

  // Field name: ytop
  {
    cdr << ros_message->ytop;
  }

  // Field name: ybot
  {
    cdr << ros_message->ybot;
  }

  // Field name: name
  {
    cdr << ros_message->name;
  }

  return true;
}

static bool _Img__cdr_deserialize(
  eprosima::fastcdr::Cdr & cdr,
  void * untyped_ros_message)
{
  if (!untyped_ros_message) {
    fprintf(stderr, "ros message handle is null\n");
    return false;
  }
  _Img__ros_msg_type * ros_message = static_cast<_Img__ros_msg_type *>(untyped_ros_message);
  // Field name: xtop
  {
    cdr >> ros_message->xtop;
  }

  // Field name: xbot
  {
    cdr >> ros_message->xbot;
  }

  // Field name: ytop
  {
    cdr >> ros_message->ytop;
  }

  // Field name: ybot
  {
    cdr >> ros_message->ybot;
  }

  // Field name: name
  {
    cdr >> ros_message->name;
  }

  return true;
}

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_messages
size_t get_serialized_size_messages__msg__Img(
  const void * untyped_ros_message,
  size_t current_alignment)
{
  const _Img__ros_msg_type * ros_message = static_cast<const _Img__ros_msg_type *>(untyped_ros_message);
  (void)ros_message;
  size_t initial_alignment = current_alignment;

  const size_t padding = 4;
  const size_t wchar_size = 4;
  (void)padding;
  (void)wchar_size;

  // field.name xtop
  {
    size_t item_size = sizeof(ros_message->xtop);
    current_alignment += item_size +
      eprosima::fastcdr::Cdr::alignment(current_alignment, item_size);
  }
  // field.name xbot
  {
    size_t item_size = sizeof(ros_message->xbot);
    current_alignment += item_size +
      eprosima::fastcdr::Cdr::alignment(current_alignment, item_size);
  }
  // field.name ytop
  {
    size_t item_size = sizeof(ros_message->ytop);
    current_alignment += item_size +
      eprosima::fastcdr::Cdr::alignment(current_alignment, item_size);
  }
  // field.name ybot
  {
    size_t item_size = sizeof(ros_message->ybot);
    current_alignment += item_size +
      eprosima::fastcdr::Cdr::alignment(current_alignment, item_size);
  }
  // field.name name
  {
    size_t item_size = sizeof(ros_message->name);
    current_alignment += item_size +
      eprosima::fastcdr::Cdr::alignment(current_alignment, item_size);
  }

  return current_alignment - initial_alignment;
}

static uint32_t _Img__get_serialized_size(const void * untyped_ros_message)
{
  return static_cast<uint32_t>(
    get_serialized_size_messages__msg__Img(
      untyped_ros_message, 0));
}

ROSIDL_TYPESUPPORT_FASTRTPS_C_PUBLIC_messages
size_t max_serialized_size_messages__msg__Img(
  bool & full_bounded,
  size_t current_alignment)
{
  size_t initial_alignment = current_alignment;

  const size_t padding = 4;
  const size_t wchar_size = 4;
  (void)padding;
  (void)wchar_size;
  (void)full_bounded;

  // member: xtop
  {
    size_t array_size = 1;

    current_alignment += array_size * sizeof(uint32_t) +
      eprosima::fastcdr::Cdr::alignment(current_alignment, sizeof(uint32_t));
  }
  // member: xbot
  {
    size_t array_size = 1;

    current_alignment += array_size * sizeof(uint32_t) +
      eprosima::fastcdr::Cdr::alignment(current_alignment, sizeof(uint32_t));
  }
  // member: ytop
  {
    size_t array_size = 1;

    current_alignment += array_size * sizeof(uint32_t) +
      eprosima::fastcdr::Cdr::alignment(current_alignment, sizeof(uint32_t));
  }
  // member: ybot
  {
    size_t array_size = 1;

    current_alignment += array_size * sizeof(uint32_t) +
      eprosima::fastcdr::Cdr::alignment(current_alignment, sizeof(uint32_t));
  }
  // member: name
  {
    size_t array_size = 1;

    current_alignment += array_size * sizeof(uint8_t);
  }

  return current_alignment - initial_alignment;
}

static size_t _Img__max_serialized_size(bool & full_bounded)
{
  return max_serialized_size_messages__msg__Img(
    full_bounded, 0);
}


static message_type_support_callbacks_t __callbacks_Img = {
  "messages::msg",
  "Img",
  _Img__cdr_serialize,
  _Img__cdr_deserialize,
  _Img__get_serialized_size,
  _Img__max_serialized_size
};

static rosidl_message_type_support_t _Img__type_support = {
  rosidl_typesupport_fastrtps_c__identifier,
  &__callbacks_Img,
  get_message_typesupport_handle_function,
};

const rosidl_message_type_support_t *
ROSIDL_TYPESUPPORT_INTERFACE__MESSAGE_SYMBOL_NAME(rosidl_typesupport_fastrtps_c, messages, msg, Img)() {
  return &_Img__type_support;
}

#if defined(__cplusplus)
}
#endif
