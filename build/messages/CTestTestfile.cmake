# CMake generated Testfile for 
# Source directory: /home/dom/Cerberus/src/messages
# Build directory: /home/dom/Cerberus/build/messages
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(lint_cmake "/usr/bin/python3" "-u" "/opt/ros/dashing/share/ament_cmake_test/cmake/run_test.py" "/home/dom/Cerberus/build/messages/test_results/messages/lint_cmake.xunit.xml" "--package-name" "messages" "--output-file" "/home/dom/Cerberus/build/messages/ament_lint_cmake/lint_cmake.txt" "--command" "/opt/ros/dashing/bin/ament_lint_cmake" "--xunit-file" "/home/dom/Cerberus/build/messages/test_results/messages/lint_cmake.xunit.xml")
set_tests_properties(lint_cmake PROPERTIES  LABELS "lint_cmake;linter" TIMEOUT "60" WORKING_DIRECTORY "/home/dom/Cerberus/src/messages")
add_test(xmllint "/usr/bin/python3" "-u" "/opt/ros/dashing/share/ament_cmake_test/cmake/run_test.py" "/home/dom/Cerberus/build/messages/test_results/messages/xmllint.xunit.xml" "--package-name" "messages" "--output-file" "/home/dom/Cerberus/build/messages/ament_xmllint/xmllint.txt" "--command" "/opt/ros/dashing/bin/ament_xmllint" "--xunit-file" "/home/dom/Cerberus/build/messages/test_results/messages/xmllint.xunit.xml")
set_tests_properties(xmllint PROPERTIES  LABELS "xmllint;linter" TIMEOUT "60" WORKING_DIRECTORY "/home/dom/Cerberus/src/messages")
subdirs("messages__py")
