#include "Img_.h"
#include "Img_Dcps.h"

namespace messages
{
    namespace msg
    {
        namespace dds_
        {
#if DDS_USE_EXPLICIT_TEMPLATES
template class DDS_DCPSUFLSeq < messages::msg::dds_::Img_, struct Img_Seq_uniq_>;
#endif

const char * messages::msg::dds_::Img_TypeSupportInterface::_local_id = "IDL:::messages::msg::dds_/Img_TypeSupportInterface:1.0";

messages::msg::dds_::Img_TypeSupportInterface_ptr messages::msg::dds_::Img_TypeSupportInterface::_duplicate (messages::msg::dds_::Img_TypeSupportInterface_ptr p)
{
    if (p) p->m_count++;
    return p;
}

DDS::Boolean messages::msg::dds_::Img_TypeSupportInterface::_local_is_a (const char * _id)
{
    if (strcmp (_id, messages::msg::dds_::Img_TypeSupportInterface::_local_id) == 0)
    {
        return true;
    }

    typedef DDS::TypeSupport NestedBase_1;

    if (NestedBase_1::_local_is_a (_id))
    {
        return true;
    }

    return false;
}

messages::msg::dds_::Img_TypeSupportInterface_ptr messages::msg::dds_::Img_TypeSupportInterface::_narrow (DDS::Object_ptr p)
{
    messages::msg::dds_::Img_TypeSupportInterface_ptr result = NULL;
    if (p && p->_is_a (messages::msg::dds_::Img_TypeSupportInterface::_local_id))
    {
        result = dynamic_cast < messages::msg::dds_::Img_TypeSupportInterface_ptr> (p);
        if (result) result->m_count++;
    }
    return result;
}

messages::msg::dds_::Img_TypeSupportInterface_ptr messages::msg::dds_::Img_TypeSupportInterface::_unchecked_narrow (DDS::Object_ptr p)
{
    messages::msg::dds_::Img_TypeSupportInterface_ptr result;
    result = dynamic_cast < messages::msg::dds_::Img_TypeSupportInterface_ptr> (p);
    if (result) result->m_count++;
    return result;
}

const char * messages::msg::dds_::Img_DataWriter::_local_id = "IDL:::messages::msg::dds_/Img_DataWriter:1.0";

messages::msg::dds_::Img_DataWriter_ptr messages::msg::dds_::Img_DataWriter::_duplicate (messages::msg::dds_::Img_DataWriter_ptr p)
{
    if (p) p->m_count++;
    return p;
}

DDS::Boolean messages::msg::dds_::Img_DataWriter::_local_is_a (const char * _id)
{
    if (strcmp (_id, messages::msg::dds_::Img_DataWriter::_local_id) == 0)
    {
        return true;
    }

    typedef DDS::DataWriter NestedBase_1;

    if (NestedBase_1::_local_is_a (_id))
    {
        return true;
    }

    return false;
}

messages::msg::dds_::Img_DataWriter_ptr messages::msg::dds_::Img_DataWriter::_narrow (DDS::Object_ptr p)
{
    messages::msg::dds_::Img_DataWriter_ptr result = NULL;
    if (p && p->_is_a (messages::msg::dds_::Img_DataWriter::_local_id))
    {
        result = dynamic_cast < messages::msg::dds_::Img_DataWriter_ptr> (p);
        if (result) result->m_count++;
    }
    return result;
}

messages::msg::dds_::Img_DataWriter_ptr messages::msg::dds_::Img_DataWriter::_unchecked_narrow (DDS::Object_ptr p)
{
    messages::msg::dds_::Img_DataWriter_ptr result;
    result = dynamic_cast < messages::msg::dds_::Img_DataWriter_ptr> (p);
    if (result) result->m_count++;
    return result;
}

const char * messages::msg::dds_::Img_DataReader::_local_id = "IDL:::messages::msg::dds_/Img_DataReader:1.0";

messages::msg::dds_::Img_DataReader_ptr messages::msg::dds_::Img_DataReader::_duplicate (messages::msg::dds_::Img_DataReader_ptr p)
{
    if (p) p->m_count++;
    return p;
}

DDS::Boolean messages::msg::dds_::Img_DataReader::_local_is_a (const char * _id)
{
    if (strcmp (_id, messages::msg::dds_::Img_DataReader::_local_id) == 0)
    {
        return true;
    }

    typedef DDS::DataReader NestedBase_1;

    if (NestedBase_1::_local_is_a (_id))
    {
        return true;
    }

    return false;
}

messages::msg::dds_::Img_DataReader_ptr messages::msg::dds_::Img_DataReader::_narrow (DDS::Object_ptr p)
{
    messages::msg::dds_::Img_DataReader_ptr result = NULL;
    if (p && p->_is_a (messages::msg::dds_::Img_DataReader::_local_id))
    {
        result = dynamic_cast < messages::msg::dds_::Img_DataReader_ptr> (p);
        if (result) result->m_count++;
    }
    return result;
}

messages::msg::dds_::Img_DataReader_ptr messages::msg::dds_::Img_DataReader::_unchecked_narrow (DDS::Object_ptr p)
{
    messages::msg::dds_::Img_DataReader_ptr result;
    result = dynamic_cast < messages::msg::dds_::Img_DataReader_ptr> (p);
    if (result) result->m_count++;
    return result;
}

const char * messages::msg::dds_::Img_DataReaderView::_local_id = "IDL:::messages::msg::dds_/Img_DataReaderView:1.0";

messages::msg::dds_::Img_DataReaderView_ptr messages::msg::dds_::Img_DataReaderView::_duplicate (messages::msg::dds_::Img_DataReaderView_ptr p)
{
    if (p) p->m_count++;
    return p;
}

DDS::Boolean messages::msg::dds_::Img_DataReaderView::_local_is_a (const char * _id)
{
    if (strcmp (_id, messages::msg::dds_::Img_DataReaderView::_local_id) == 0)
    {
        return true;
    }

    typedef DDS::DataReaderView NestedBase_1;

    if (NestedBase_1::_local_is_a (_id))
    {
        return true;
    }

    return false;
}

messages::msg::dds_::Img_DataReaderView_ptr messages::msg::dds_::Img_DataReaderView::_narrow (DDS::Object_ptr p)
{
    messages::msg::dds_::Img_DataReaderView_ptr result = NULL;
    if (p && p->_is_a (messages::msg::dds_::Img_DataReaderView::_local_id))
    {
        result = dynamic_cast < messages::msg::dds_::Img_DataReaderView_ptr> (p);
        if (result) result->m_count++;
    }
    return result;
}

messages::msg::dds_::Img_DataReaderView_ptr messages::msg::dds_::Img_DataReaderView::_unchecked_narrow (DDS::Object_ptr p)
{
    messages::msg::dds_::Img_DataReaderView_ptr result;
    result = dynamic_cast < messages::msg::dds_::Img_DataReaderView_ptr> (p);
    if (result) result->m_count++;
    return result;
}

        }

    }

}

