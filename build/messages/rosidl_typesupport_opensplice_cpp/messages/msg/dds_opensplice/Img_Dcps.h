#ifndef _H_36B68CA0F2C9331561F0FF773C898AF3_Img_DCPS_H_
#define _H_36B68CA0F2C9331561F0FF773C898AF3_Img_DCPS_H_

#include "sacpp_mapping.h"
#include "dds_dcps.h"
#include "Img_.h"


namespace messages
{
    namespace msg
    {
        namespace dds_
        {
            class Img_TypeSupportInterface;

            typedef Img_TypeSupportInterface * Img_TypeSupportInterface_ptr;
            typedef DDS_DCPSInterface_var < Img_TypeSupportInterface> Img_TypeSupportInterface_var;
            typedef DDS_DCPSInterface_out < Img_TypeSupportInterface> Img_TypeSupportInterface_out;


            class Img_DataWriter;

            typedef Img_DataWriter * Img_DataWriter_ptr;
            typedef DDS_DCPSInterface_var < Img_DataWriter> Img_DataWriter_var;
            typedef DDS_DCPSInterface_out < Img_DataWriter> Img_DataWriter_out;


            class Img_DataReader;

            typedef Img_DataReader * Img_DataReader_ptr;
            typedef DDS_DCPSInterface_var < Img_DataReader> Img_DataReader_var;
            typedef DDS_DCPSInterface_out < Img_DataReader> Img_DataReader_out;


            class Img_DataReaderView;

            typedef Img_DataReaderView * Img_DataReaderView_ptr;
            typedef DDS_DCPSInterface_var < Img_DataReaderView> Img_DataReaderView_var;
            typedef DDS_DCPSInterface_out < Img_DataReaderView> Img_DataReaderView_out;

            struct Img_Seq_uniq_ {};
            typedef DDS_DCPSUFLSeq < Img_, struct Img_Seq_uniq_> Img_Seq;
            typedef DDS_DCPSSequence_var < Img_Seq> Img_Seq_var;
            typedef DDS_DCPSSequence_out < Img_Seq> Img_Seq_out;

            class  Img_TypeSupportInterface :
                virtual public DDS::TypeSupport
            { 
            public:
                typedef Img_TypeSupportInterface_ptr _ptr_type;
                typedef Img_TypeSupportInterface_var _var_type;

                static Img_TypeSupportInterface_ptr _duplicate (Img_TypeSupportInterface_ptr obj);
                DDS::Boolean _local_is_a (const char * id);

                static Img_TypeSupportInterface_ptr _narrow (DDS::Object_ptr obj);
                static Img_TypeSupportInterface_ptr _unchecked_narrow (DDS::Object_ptr obj);
                static Img_TypeSupportInterface_ptr _nil () { return 0; }
                static const char * _local_id;
                Img_TypeSupportInterface_ptr _this () { return this; }


            protected:
                Img_TypeSupportInterface () {};
                ~Img_TypeSupportInterface () {};
            private:
                Img_TypeSupportInterface (const Img_TypeSupportInterface &);
                Img_TypeSupportInterface & operator = (const Img_TypeSupportInterface &);
            };

            class  Img_DataWriter :
                virtual public DDS::DataWriter
            { 
            public:
                typedef Img_DataWriter_ptr _ptr_type;
                typedef Img_DataWriter_var _var_type;

                static Img_DataWriter_ptr _duplicate (Img_DataWriter_ptr obj);
                DDS::Boolean _local_is_a (const char * id);

                static Img_DataWriter_ptr _narrow (DDS::Object_ptr obj);
                static Img_DataWriter_ptr _unchecked_narrow (DDS::Object_ptr obj);
                static Img_DataWriter_ptr _nil () { return 0; }
                static const char * _local_id;
                Img_DataWriter_ptr _this () { return this; }

                virtual DDS::LongLong register_instance (const Img_& instance_data) = 0;
                virtual DDS::LongLong register_instance_w_timestamp (const Img_& instance_data, const DDS::Time_t& source_timestamp) = 0;
                virtual DDS::Long unregister_instance (const Img_& instance_data, DDS::LongLong handle) = 0;
                virtual DDS::Long unregister_instance_w_timestamp (const Img_& instance_data, DDS::LongLong handle, const DDS::Time_t& source_timestamp) = 0;
                virtual DDS::Long write (const Img_& instance_data, DDS::LongLong handle) = 0;
                virtual DDS::Long write_w_timestamp (const Img_& instance_data, DDS::LongLong handle, const DDS::Time_t& source_timestamp) = 0;
                virtual DDS::Long dispose (const Img_& instance_data, DDS::LongLong handle) = 0;
                virtual DDS::Long dispose_w_timestamp (const Img_& instance_data, DDS::LongLong handle, const DDS::Time_t& source_timestamp) = 0;
                virtual DDS::Long writedispose (const Img_& instance_data, DDS::LongLong handle) = 0;
                virtual DDS::Long writedispose_w_timestamp (const Img_& instance_data, DDS::LongLong handle, const DDS::Time_t& source_timestamp) = 0;
                virtual DDS::Long get_key_value (Img_& key_holder, DDS::LongLong handle) = 0;
                virtual DDS::LongLong lookup_instance (const Img_& instance_data) = 0;

            protected:
                Img_DataWriter () {};
                ~Img_DataWriter () {};
            private:
                Img_DataWriter (const Img_DataWriter &);
                Img_DataWriter & operator = (const Img_DataWriter &);
            };

            class  Img_DataReader :
                virtual public DDS::DataReader
            { 
            public:
                typedef Img_DataReader_ptr _ptr_type;
                typedef Img_DataReader_var _var_type;

                static Img_DataReader_ptr _duplicate (Img_DataReader_ptr obj);
                DDS::Boolean _local_is_a (const char * id);

                static Img_DataReader_ptr _narrow (DDS::Object_ptr obj);
                static Img_DataReader_ptr _unchecked_narrow (DDS::Object_ptr obj);
                static Img_DataReader_ptr _nil () { return 0; }
                static const char * _local_id;
                Img_DataReader_ptr _this () { return this; }

                virtual DDS::Long read (Img_Seq& received_data, DDS::SampleInfoSeq& info_seq, DDS::Long max_samples, DDS::ULong sample_states, DDS::ULong view_states, DDS::ULong instance_states) = 0;
                virtual DDS::Long take (Img_Seq& received_data, DDS::SampleInfoSeq& info_seq, DDS::Long max_samples, DDS::ULong sample_states, DDS::ULong view_states, DDS::ULong instance_states) = 0;
                virtual DDS::Long read_w_condition (Img_Seq& received_data, DDS::SampleInfoSeq& info_seq, DDS::Long max_samples, DDS::ReadCondition_ptr a_condition) = 0;
                virtual DDS::Long take_w_condition (Img_Seq& received_data, DDS::SampleInfoSeq& info_seq, DDS::Long max_samples, DDS::ReadCondition_ptr a_condition) = 0;
                virtual DDS::Long read_next_sample (Img_& received_data, DDS::SampleInfo& sample_info) = 0;
                virtual DDS::Long take_next_sample (Img_& received_data, DDS::SampleInfo& sample_info) = 0;
                virtual DDS::Long read_instance (Img_Seq& received_data, DDS::SampleInfoSeq& info_seq, DDS::Long max_samples, DDS::LongLong a_handle, DDS::ULong sample_states, DDS::ULong view_states, DDS::ULong instance_states) = 0;
                virtual DDS::Long take_instance (Img_Seq& received_data, DDS::SampleInfoSeq& info_seq, DDS::Long max_samples, DDS::LongLong a_handle, DDS::ULong sample_states, DDS::ULong view_states, DDS::ULong instance_states) = 0;
                virtual DDS::Long read_next_instance (Img_Seq& received_data, DDS::SampleInfoSeq& info_seq, DDS::Long max_samples, DDS::LongLong a_handle, DDS::ULong sample_states, DDS::ULong view_states, DDS::ULong instance_states) = 0;
                virtual DDS::Long take_next_instance (Img_Seq& received_data, DDS::SampleInfoSeq& info_seq, DDS::Long max_samples, DDS::LongLong a_handle, DDS::ULong sample_states, DDS::ULong view_states, DDS::ULong instance_states) = 0;
                virtual DDS::Long read_next_instance_w_condition (Img_Seq& received_data, DDS::SampleInfoSeq& info_seq, DDS::Long max_samples, DDS::LongLong a_handle, DDS::ReadCondition_ptr a_condition) = 0;
                virtual DDS::Long take_next_instance_w_condition (Img_Seq& received_data, DDS::SampleInfoSeq& info_seq, DDS::Long max_samples, DDS::LongLong a_handle, DDS::ReadCondition_ptr a_condition) = 0;
                virtual DDS::Long return_loan (Img_Seq& received_data, DDS::SampleInfoSeq& info_seq) = 0;
                virtual DDS::Long get_key_value (Img_& key_holder, DDS::LongLong handle) = 0;
                virtual DDS::LongLong lookup_instance (const Img_& instance) = 0;

            protected:
                Img_DataReader () {};
                ~Img_DataReader () {};
            private:
                Img_DataReader (const Img_DataReader &);
                Img_DataReader & operator = (const Img_DataReader &);
            };

            class  Img_DataReaderView :
                virtual public DDS::DataReaderView
            { 
            public:
                typedef Img_DataReaderView_ptr _ptr_type;
                typedef Img_DataReaderView_var _var_type;

                static Img_DataReaderView_ptr _duplicate (Img_DataReaderView_ptr obj);
                DDS::Boolean _local_is_a (const char * id);

                static Img_DataReaderView_ptr _narrow (DDS::Object_ptr obj);
                static Img_DataReaderView_ptr _unchecked_narrow (DDS::Object_ptr obj);
                static Img_DataReaderView_ptr _nil () { return 0; }
                static const char * _local_id;
                Img_DataReaderView_ptr _this () { return this; }

                virtual DDS::Long read (Img_Seq& received_data, DDS::SampleInfoSeq& info_seq, DDS::Long max_samples, DDS::ULong sample_states, DDS::ULong view_states, DDS::ULong instance_states) = 0;
                virtual DDS::Long take (Img_Seq& received_data, DDS::SampleInfoSeq& info_seq, DDS::Long max_samples, DDS::ULong sample_states, DDS::ULong view_states, DDS::ULong instance_states) = 0;
                virtual DDS::Long read_w_condition (Img_Seq& received_data, DDS::SampleInfoSeq& info_seq, DDS::Long max_samples, DDS::ReadCondition_ptr a_condition) = 0;
                virtual DDS::Long take_w_condition (Img_Seq& received_data, DDS::SampleInfoSeq& info_seq, DDS::Long max_samples, DDS::ReadCondition_ptr a_condition) = 0;
                virtual DDS::Long read_next_sample (Img_& received_data, DDS::SampleInfo& sample_info) = 0;
                virtual DDS::Long take_next_sample (Img_& received_data, DDS::SampleInfo& sample_info) = 0;
                virtual DDS::Long read_instance (Img_Seq& received_data, DDS::SampleInfoSeq& info_seq, DDS::Long max_samples, DDS::LongLong a_handle, DDS::ULong sample_states, DDS::ULong view_states, DDS::ULong instance_states) = 0;
                virtual DDS::Long take_instance (Img_Seq& received_data, DDS::SampleInfoSeq& info_seq, DDS::Long max_samples, DDS::LongLong a_handle, DDS::ULong sample_states, DDS::ULong view_states, DDS::ULong instance_states) = 0;
                virtual DDS::Long read_next_instance (Img_Seq& received_data, DDS::SampleInfoSeq& info_seq, DDS::Long max_samples, DDS::LongLong a_handle, DDS::ULong sample_states, DDS::ULong view_states, DDS::ULong instance_states) = 0;
                virtual DDS::Long take_next_instance (Img_Seq& received_data, DDS::SampleInfoSeq& info_seq, DDS::Long max_samples, DDS::LongLong a_handle, DDS::ULong sample_states, DDS::ULong view_states, DDS::ULong instance_states) = 0;
                virtual DDS::Long read_next_instance_w_condition (Img_Seq& received_data, DDS::SampleInfoSeq& info_seq, DDS::Long max_samples, DDS::LongLong a_handle, DDS::ReadCondition_ptr a_condition) = 0;
                virtual DDS::Long take_next_instance_w_condition (Img_Seq& received_data, DDS::SampleInfoSeq& info_seq, DDS::Long max_samples, DDS::LongLong a_handle, DDS::ReadCondition_ptr a_condition) = 0;
                virtual DDS::Long return_loan (Img_Seq& received_data, DDS::SampleInfoSeq& info_seq) = 0;
                virtual DDS::Long get_key_value (Img_& key_holder, DDS::LongLong handle) = 0;
                virtual DDS::LongLong lookup_instance (const Img_& instance) = 0;

            protected:
                Img_DataReaderView () {};
                ~Img_DataReaderView () {};
            private:
                Img_DataReaderView (const Img_DataReaderView &);
                Img_DataReaderView & operator = (const Img_DataReaderView &);
            };
        }

    }

}

#endif
