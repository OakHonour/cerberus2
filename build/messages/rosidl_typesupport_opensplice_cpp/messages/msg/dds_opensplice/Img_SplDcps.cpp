#include "Img_SplDcps.h"
#include "ccpp_Img_.h"

#include <v_copyIn.h>
#include <v_topic.h>
#include <os_stdlib.h>
#include <string.h>
#include <os_report.h>

v_copyin_result
__messages_msg_dds__Img___copyIn(
    c_base base,
    const struct ::messages::msg::dds_::Img_ *from,
    struct _messages_msg_dds__Img_ *to)
{
    v_copyin_result result = V_COPYIN_RESULT_OK;
    (void) base;

    to->xtop_ = (c_float)from->xtop_;
    to->xbot_ = (c_float)from->xbot_;
    to->ytop_ = (c_float)from->ytop_;
    to->ybot_ = (c_float)from->ybot_;
    to->name_ = (c_octet)from->name_;
    return result;
}

void
__messages_msg_dds__Img___copyOut(
    const void *_from,
    void *_to)
{
    const struct _messages_msg_dds__Img_ *from = (const struct _messages_msg_dds__Img_ *)_from;
    struct ::messages::msg::dds_::Img_ *to = (struct ::messages::msg::dds_::Img_ *)_to;
    to->xtop_ = (::DDS::Float)from->xtop_;
    to->xbot_ = (::DDS::Float)from->xbot_;
    to->ytop_ = (::DDS::Float)from->ytop_;
    to->ybot_ = (::DDS::Float)from->ybot_;
    to->name_ = (::DDS::Octet)from->name_;
}

