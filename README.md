## Cerberus Human Detection Robot

Code for a robot designed to identify and follow humans. The current code allows the robot 
to identify a person using an implementation of yoloV3. The robot will pivot (on wheels) to keep a single detected person within it's field of view.

- YoloV3 object detector written in python. Weights and biases from coco trained model.
- Robot control written in C++.
- ROS2 used for communications framework.
